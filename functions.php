<?php

// Load all css to render examples and style of components

function getAllCSS() {

$dir = "markup/ui-css/";

// Open a directory, and read its contents
if (is_dir($dir)){
  if ($dh = opendir($dir)){
    while (($file = readdir($dh)) !== false){
		if ($file !== '.' && $file !== '..') {
			echo '<link rel="stylesheet" href="'.$dir.''.$file.'">';	
		}
    }
    closedir($dh);
  }
}


}


// render the requiredjavascript and css to operate expanding code divs
function prerendercode($topic) {



 	$arrlength = count($topic);
    
    echo '
   	<script type=\'text/javascript\'>
	$(window).load(function(){';
    
    for($x = 0; $x < $arrlength; $x++) {    
		echo '	
		$(\'.expand-'.$topic[$x].'winhtml\').click(function(){
    		$(\'.'.$topic[$x].'winhtml\').slideToggle(\'slow\');
		});
		
		$(\'.expand-'.$topic[$x].'wincss\').click(function(){
    		$(\'.'.$topic[$x].'wincss\').slideToggle(\'slow\');
		});
		
		$(\'.expand-'.$topic[$x].'winless\').click(function(){
    		$(\'.'.$topic[$x].'winless\').slideToggle(\'slow\');
		});
        
        $(\'.expand-'.$topic[$x].'winjs\').click(function(){
    		$(\'.'.$topic[$x].'winjs\').slideToggle(\'slow\');
		});
		
		';
	}	
	echo '});';
	echo '</script>';
	
	
	echo '<style>';
	
	for($x = 0; $x < $arrlength; $x++) {
	    echo ' 
		.'.$topic[$x].'wincss {
    	display:none;
    	height: 100%;
    	max-height: 400px;
    	overflow:scroll;
    	background-color: #fff;
		}
		.'.$topic[$x].'winhtml {
    	display:none;
    	height: 100%;
    	max-height: 400px;
    	overflow:scroll;
    	background-color: #fff;
		}
		.'.$topic[$x].'winless {
    	display:none;
    	height: 100%;
    	max-height: 400px;
    	overflow:scroll;
    	background-color: #fff;
		}
        .'.$topic[$x].'winjs {
    	display:none;
    	height: 100%;
    	max-height: 400px;
    	overflow:scroll;
    	background-color: #fff;
		}
		';
	}	
	
	echo '</style>';
	
}





// Render HTML, CSS and LESS code


function getContent($doc) {	
	
	renderFileDoc($doc);	
}


function renderFileDoc($doc) {
	
	
	$path = 'markup/ui-html/'.$doc.'.html';

	// Render HTML	
	$panelname = $doc;
	
    require_once 'Parsedown.php';
    $Parsedown = new Parsedown();

    // Check if markdown doc exists
    if (is_readable($path)) {
		//echo '<div id="'.$panelname.'htmlcode" class="expand-bar">'; // if you want the page to scroll to this unique position
		echo '<div class="expand-bar">';
			echo '<p class="expand-'.$panelname.'winhtml"><a href="#'.$panelname.'htmlcode">HTML Code</a><p>';
		echo '</div>';		
		
      echo '<div class="'.$panelname.'winhtml codewindow">';
	  
      $markupText = $Parsedown->text(file_get_contents($path));
      echo '<pre><code class="language-markup">';
	  $markupText = str_replace('<pre>','',$markupText);
	  $markupText = str_replace('</pre>','',$markupText);
	  $markupText = str_replace('<code>','',$markupText);
	  $markupText = str_replace('</code>','',$markupText);
      $markupText = str_replace('<','&lt;',$markupText);
	  echo $markupText;
	  echo '</code></pre>';
	  echo '</div>';
	  
    }
	
	
	// Render CSS
	
	$cssdoc = str_replace(".html",".css",$path);
	$cssdoc = str_replace("html","css",$cssdoc);
	$CSStext = file_get_contents($cssdoc);

	
	if (is_readable($cssdoc)) {	
	
	//echo '<div id="'.$panelname.'csscode" class="expand-bar">'; // if you want the page to scroll to this unique position
	echo '<div class="expand-bar">';
	echo '<p class="expand-'.$panelname.'wincss"><a href="#'.$panelname.'csscode">CSS Code</a><p>';
	echo '</div>';	
	
	echo '<div class="'.$panelname.'wincss codewindow">';
	echo '<div class="markdown-body"><pre><code class=language-css>';
		echo $CSStext;
	echo '</code></pre></div>';
    echo '</div>';
	}
	
	
	
	// Render LESS
	
	$cssdoc = str_replace(".html",".less",$path);
	$cssdoc = str_replace("html","less",$cssdoc);

	$LESStext = file_get_contents($cssdoc);
	
	if (is_readable($cssdoc)) {
	// echo '<div id="'.$panelname.'lesscode" class="expand-bar">'; // if you want the page to scroll to this unique position
	echo '<div class="expand-bar">';
	echo '<p class="expand-'.$panelname.'winless"><a href="#'.$panelname.'lesscode">Less Code</a><p>';
	echo '</div>';
		
	echo '<div class="'.$panelname.'winless codewindow">';
	echo '<div class="markdown-body winless"><pre><code class=language-less>';
		echo $LESStext;
	echo '</code></pre></div>';
    echo '</div>';
	}
    
    // Render JS
	
	$cssdoc = str_replace(".html",".js",$path);
	$cssdoc = str_replace("html","js",$cssdoc);

	$JStext = file_get_contents($cssdoc);
	
	if (is_readable($cssdoc)) {
	// echo '<div id="'.$panelname.'lesscode" class="expand-bar">'; // if you want the page to scroll to this unique position
	echo '<div class="expand-bar">';
	echo '<p class="expand-'.$panelname.'winjs"><a href="#'.$panelname.'jscode">JS Code</a><p>';
	echo '</div>';
		
	echo '<div class="'.$panelname.'winjs codewindow">';
	echo '<div class="markdown-body winjs"><pre><code class=language-less>';
		echo $JStext;
	echo '</code></pre></div>';
    echo '</div>';
	}    
    
	
	
}


?>
