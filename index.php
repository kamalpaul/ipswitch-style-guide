<!DOCTYPE html>
<html lang="en">

<head>
   	<?php include('template-header.html'); ?>
</head>

<body hoe-navigation-type="vertical" hoe-nav-placement="left" theme-layout="wide-layout" theme-bg="bg1" >
    <div id="hoeapp-wrapper" class="hoe-hide-lpanel" hoe-device-type="desktop">
        <div class="topheader">  		
        <header id="hoe-header" hoe-lpanel-effect="shrink">
      		
      		<?php include('leftheader.php'); ?>
      		
      		<?php include('rightheader.php'); ?>
			
		</header>
        </div>
        <div id="hoeapp-container" hoe-color-type="lpanel-bg2" hoe-lpanel-effect="shrink">
           
            <?php include('nav.php'); ?>
            
            <section id="main-content">
            <?php // ************* CONTENT GOES HERE *************** ?>
            
			<?php include('markup/introduction/introduction.html'); ?>
				
        	<?php // *********************************************** ?>     
            </section><!-- end main-content-->
            
		</div><!-- end hoeapp-container-->
    </div><!-- end hoeapp-wrapper-->
  
 	<?php include('template-endbody.html'); ?> 
  
</body>

</html>