<!--ZOOMSTOP-->
<aside id="hoe-left-panel" hoe-position-type="absolute">
                <ul class="nav panel-list styleguidenav">
                	<li>
                        <a class="action" href="index.php">
                            <i class="fa fa-home"></i>
                            <span class="menu-text">Introduction</span>
                            <span class="selected"></span>
                        </a>
                    </li>    
                
                    <li class="hoe-has-menu">
                        <a href="javascript:void(0)">
                            <i class="fa fa-paint-brush"></i>
                            <span class="menu-text">Style</span>
                            <span class="selected"></span>
                        </a>
                        <ul class="hoe-sub-menu">
                        
                       		<?php
							$dir = "markup/style/";
							if ($handle = opendir($dir )) {
								while (false !== ($entry = readdir($handle))) {
									if ($entry != "." && $entry != ".." && $entry != ".DS_Store") {
										
										// remove html from name
										$page = str_replace(".html","",$entry);
										//echo $page.' ';
										
										// replace underscores _ with spaces, sentence case title
										$title = str_replace("_"," ",$page);
										$title = ucwords($title);
										$title = str_replace("And","and",$title);
										//echo $title.' <br />';
										
										echo '
										
										<li>
                                			<a class="action" href="topic.php?title='.$title.'&category=style&topic='.$page.'">
                                    		<span class="menu-text">'.$title.'</span>
                                    		<span class="selected"></span>
                                			</a>
                            			</li> 
									
										';
									
									}
								}
								closedir($handle);
							}
							?>
                            
                        </ul>
                    </li> 
                    <li class="hoe-has-menu">
                        <a href="javascript:void(0)">
                            <i class="fa fa-columns"></i>
                            <span class="menu-text">Layout</span>
                            <span class="selected"></span>
                        </a>
                        <ul class="hoe-sub-menu">
                            
                            
						<?php
							$dir = "markup/layout/";
							if ($handle = opendir($dir )) {
								while (false !== ($entry = readdir($handle))) {
									if ($entry != "." && $entry != ".." && $entry != ".DS_Store") {
										
										// remove html from name
										$page = str_replace(".html","",$entry);
										//echo $page.' ';
										
										// replace underscores _ with spaces, sentence case title
										$title = str_replace("_"," ",$page);
										$title = ucwords($title);
										$title = str_replace("And","and",$title);
										//echo $title.' <br />';
										
										echo '
										
										<li>
                                			<a class="action" href="topic.php?title='.$title.'&category=layout&topic='.$page.'">
                                    		<span class="menu-text">'.$title.'</span>
                                    		<span class="selected"></span>
                                			</a>
                            			</li> 
									
										';
									
									}
								}
								closedir($handle);
							}
							?>                            
                            
                            
                            
                        </ul>
                    </li>
                    <li class="hoe-has-menu">
                        <a href="javascript:void(0)">
                            <i class="fa fa-toggle-on"></i>
                            <span class="menu-text">Components</span>
                            <span class="selected"></span>
                        </a>
                        <ul class="hoe-sub-menu"> 
                            
							<?php
							$dir = "markup/components/";
							if ($handle = opendir($dir )) {
								while (false !== ($entry = readdir($handle))) {
									if ($entry != "." && $entry != ".." && $entry != ".DS_Store") {
										
										// remove html from name
										$page = str_replace(".html","",$entry);
										//echo $page.' ';
										
										// replace underscores _ with spaces, sentence case title
										$title = str_replace("_"," ",$page);
										$title = ucwords($title);
										$title = str_replace("And","and",$title);
										//echo $title.' <br />';
										
										echo '
										
										<li>
                                			<a class="action" href="topic.php?title='.$title.'&category=components&topic='.$page.'">
                                    		<span class="menu-text">'.$title.'</span>
                                    		<span class="selected"></span>
                                			</a>
                            			</li> 
									
										';
									
									}
								}
								closedir($handle);
							}
							?>                             
                            
                            
                            
                            
                            
                        </ul>
                    </li>
                    <li class="hoe-has-menu">
                        <a href="javascript:void(0)">
                            <i class="fa fa-th-large"></i>
                            <span class="menu-text">Patterns</span>
                            <span class="selected"></span>
                        </a>
                        <ul class="hoe-sub-menu">
                            
							<?php
							$dir = "markup/patterns/";
							if ($handle = opendir($dir )) {
								while (false !== ($entry = readdir($handle))) {
									if ($entry != "." && $entry != ".." && $entry != ".DS_Store") {
										
										// remove html from name
										$page = str_replace(".html","",$entry);
										//echo $page.' ';
										
										// replace underscores _ with spaces, sentence case title
										$title = str_replace("_"," ",$page);
										$title = ucwords($title);
										$title = str_replace("And","and",$title);
										//echo $title.' <br />';
										
										echo '
										
										<li>
                                			<a class="action" href="topic.php?title='.$title.'&category=patterns&topic='.$page.'">
                                    		<span class="menu-text">'.$title.'</span>
                                    		<span class="selected"></span>
                                			</a>
                            			</li> 
									
										';
									
									}
								}
								closedir($handle);
							}
							?>                            
                            
                            
                        </ul>
                    </li>
                    <li class="hoe-has-menu">
                        <a href="javascript:void(0)">
                            <i class="fa fa-clone"></i>
                            <span class="menu-text">Templates</span>
                            <span class="selected"></span>
                        </a>
                        <ul class="hoe-sub-menu">
                            
							<?php
							$dir = "markup/templates/";
							if ($handle = opendir($dir )) {
								while (false !== ($entry = readdir($handle))) {
									if ($entry != "." && $entry != ".." && $entry != ".DS_Store") {
										
										// remove html from name
										$page = str_replace(".html","",$entry);
										//echo $page.' ';
										
										// replace underscores _ with spaces, sentence case title
										$title = str_replace("_"," ",$page);
										$title = ucwords($title);
										$title = str_replace("And","and",$title);
										//echo $title.' <br />';
										
										echo '
										
										<li>
                                			<a class="action" href="topic.php?title='.$title.'&category=templates&topic='.$page.'">
                                    		<span class="menu-text">'.$title.'</span>
                                    		<span class="selected"></span>
                                			</a>
                            			</li> 
									
										';
									
									}
								}
								closedir($handle);
							}
							?>                            
                            
                        </ul>
                    </li>
                </ul>
</aside>
<!--ZOOMRESTART-->