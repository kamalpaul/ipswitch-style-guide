<!DOCTYPE html>
<html lang="en">

<head>
   	<?php include('template-header.html'); ?>
    
	<!-- 
	You can change the fonts, colors, and styles of your search results with the CSS below.
	For some examples and more information on Cascading Style Sheets (CSS), visit our support page at:
	http://www.wrensoft.com/zoom/support/css.html
	-->
	<style type="text/css">
		.highlight { background: #FFFF40; }
		.searchheading { font-size: 130%; font-weight: bold; }
		.summary { font-style: italic; }
		.suggestion { font-size: 100%; }
		.results { font-size: 100%; }
		.category { color: #999999; }
		.sorting { text-align: right; }

		.result_title { font-size: 100%; }		
		.description { font-size: 100%; color: #008000; }
		.context { font-size: 100%; }
		.infoline { font-size: 80%; font-style: normal; color: #808080;}

		.zoom_searchform { font-size: 100%; }
		.zoom_results_per_page { font-weight: normal; margin-left: 10px; }
		.zoom_match { font-size: 100%;}				
		.zoom_categories { font-size: 80%; }
		.zoom_categories ul { display: inline; margin: 0px; padding: 0px;}
		.zoom_categories li { display: inline; margin-left: 15px; list-style-type: none; }
        
        #zoom_match_any {
            margin-right: 5px;
            font-weight: normal;
        }
        
        #zoom_match_any input {
            font-weight: normal;
        }
        
        #zoom_match_all {
            margin-right: 5px;
            font-weight: normal;
        }
		
		.cat_summary ul { margin: 0px; padding: 0px; display: inline; }
		.cat_summary li { display: inline; margin-left: 15px; list-style-type: none; }			
		
		.result_image { float: left; display: block; }
		.result_image img { margin: 10px; width: 80px; border: 0px; }

		.result_block { margin-top: 15px; margin-bottom: 15px; clear: left; }
		.result_altblock { margin-top: 15px; margin-bottom: 15px; clear: left; }
		
		.result_pages { font-size: 100%; }
		.result_pagescount { font-size: 100%; }
		
		.searchtime { font-size: 80%; }
		
		.recommended 
		{ 
			background: #DFFFBF; 
			border-top: 1px dotted #808080; 
			border-bottom: 1px dotted #808080; 
			margin-top: 15px; 
			margin-bottom: 15px; 
		}
		.recommended_heading { float: right; font-weight: bold; }
		.recommend_block { margin-top: 15px; margin-bottom: 15px; clear: left; }		
		.recommend_title { font-size: 100%; }
		.recommend_description { font-size: 100%; color: #008000; }
		.recommend_infoline { font-size: 80%; font-style: normal; color: #808080;}
		.recommend_image { float: left; display: block; }
		.recommend_image img { margin: 10px; width: 80px; border: 0px; }
        
        input.zoom_button {
            
            background: #fff;
            border: 1px solid #afb8bc;
            border-radius: 3px;
            font-size: 12px;
            padding-left: 10px;
            padding-right: 10px;
            height: 32px;
            display: inline;
        }
        
        input.zoom_searchbox {
            border-radius: 3px;            
            border: 1px solid #afb8bc;
            height: 32px;
            width: 200px;
            display: inline;
        }
        
	</style>    
    
</head>

<body hoe-navigation-type="vertical" hoe-nav-placement="left" theme-layout="wide-layout" theme-bg="bg1" >
    <div id="hoeapp-wrapper" class="hoe-hide-lpanel" hoe-device-type="desktop">
        <header id="hoe-header" hoe-lpanel-effect="shrink">
      		
      		<?php include('leftheader.php'); ?>
      		
      		<?php include('rightheader.php'); ?>
			
		</header>
        <div id="hoeapp-container" hoe-color-type="lpanel-bg2" hoe-lpanel-effect="shrink">
           
            <?php include('nav.php'); ?>
            
            <section id="main-content">
                <div class="container-fluid">
                    <div class="row">
			      		<div class="col-lg-1"></div>
			      		<div class="col-lg-7">
    
            <?php // ************* CONTENT GOES HERE *************** ?>
            
			<?php include('searchip.php'); ?>
				
        	<?php // *********************************************** ?>
                    
                        </div>        
                        <div class="col-lg-1"></div>
                    </div>        
                </div>
            </section><!-- end main-content-->
            
		</div><!-- end hoeapp-container-->
    </div><!-- end hoeapp-wrapper-->
  
 	<?php include('template-endbody.html'); ?> 
  
</body>

</html>