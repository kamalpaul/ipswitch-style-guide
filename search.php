<!DOCTYPE html>
<html lang="en">

<head>
   	<?php include('template-header.html'); ?>
    
	<!-- 
	You can change the fonts, colors, and styles of your search results with the CSS below.
	For some examples and more information on Cascading Style Sheets (CSS), visit our support page at:
	http://www.wrensoft.com/zoom/support/css.html
	-->
    <link href="css/zoom_search.css" rel="stylesheet">

        
</head>

<body hoe-navigation-type="vertical" hoe-nav-placement="left" theme-layout="wide-layout" theme-bg="bg1" >
    <div id="hoeapp-wrapper" class="hoe-hide-lpanel" hoe-device-type="desktop">
        <div class="topheader">  		
        <header id="hoe-header" hoe-lpanel-effect="shrink">
      		
      		<?php include('leftheader.php'); ?>
      		
      		<?php include('rightheader.php'); ?>
			
		</header>
        </div>
        <div id="hoeapp-container" hoe-color-type="lpanel-bg2" hoe-lpanel-effect="shrink">
           
            <?php include('nav.php'); ?>
            
            <section id="main-content">
                <div class="container-fluid">
                    <div class="row title-row">
			      		<div class="col-lg-1"></div>
			      		<div class="col-lg-10">
    
            <?php // ************* CONTENT GOES HERE *************** ?>
            
			<?php include('searchip.php'); ?>
				
        	<?php // *********************************************** ?>
                    
                        </div>        
                        <div class="col-lg-1"></div>
                    </div>        
                </div>
            </section><!-- end main-content-->
            
		</div><!-- end hoeapp-container-->
    </div><!-- end hoeapp-wrapper-->
  
 	<?php include('template-endbody.html'); ?> 
  
</body>

</html>