<?php include_once('functions.php'); ?>
<!DOCTYPE html>
<html lang="en">

<head>

	<?php include('template-header.html'); ?>

   
    <!-- Get All CSS for Rendering UI-->
    <?php getAllCSS(); // acquire additional css for special pages like typography and iconography ?>

    <!-- Set Javascript and Style for rendering code-->    
    <?php
    
	// render the required javascript and css to operate expanding code divs
    // this should be the same name as the html, css and less files
    // needed for prism.js
	
	$topic = array();
	
	$dir = "markup/ui-html/";
	if ($handle = opendir($dir )) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != ".." && $entry != ".DS_Store") {
				
				// remove html from name
				$page = str_replace(".html","",$entry);
				
				array_push($topic, $page);
			
			}
		}
		closedir($handle);
	}
	
	$dir = "markup/ui-css/";
	if ($handle = opendir($dir )) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != ".." && $entry != ".DS_Store") {
				
				// remove html from name
				$page = str_replace(".css","",$entry);
				
				if (in_array($page, $topic)) {
				} else {
				array_push($topic, $page);
				}
			
			}
		}
		closedir($handle);
	}
	
	$dir = "markup/ui-less/";
	if ($handle = opendir($dir )) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != ".." && $entry != ".DS_Store") {
				
				// remove html from name
				$page = str_replace(".less","",$entry);
				
				if (in_array($page, $topic)) {
				} else {
				array_push($topic, $page);
				}			
			}
		}
		closedir($handle);
	}
	
	$dir = "markup/ui-js/";
	if ($handle = opendir($dir )) {
		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != ".." && $entry != ".DS_Store") {
				
				// remove html from name
				$page = str_replace(".js","",$entry);
				
				if (in_array($page, $topic)) {
				} else {
				array_push($topic, $page);
				}			
			}
		}
		closedir($handle);
	}
	
		
	prerendercode ($topic);   
	    
    ?>
    
    
	
</head>

<body hoe-navigation-type="vertical" hoe-nav-placement="left" theme-layout="wide-layout" theme-bg="bg1" id="top">
    <div id="hoeapp-wrapper" class="hoe-hide-lpanel" hoe-device-type="desktop">
        <div class="topheader">  		
        <header id="hoe-header" hoe-lpanel-effect="shrink">
      		<?php include('leftheader.php'); ?>
            
      		<?php include('rightheader.php'); ?>    
        </header>
        </div>    
        <div id="hoeapp-container" hoe-color-type="lpanel-bg2" hoe-lpanel-effect="shrink">
            
        	<?php include('nav.php'); ?>
            
            <section id="main-content">
            <?php // ************* CONTENT GOES HERE *************** ?>
            
            	<?php
            	
            	$filename = 'markup/'.$_GET['category'].'/'.$_GET['topic'].'.html';
            	
            	if (file_exists($filename)) {
            	
            		include ($filename);
            	
            	}
            	
            	
            	?>
				
			<?php // *********************************************** ?>
            </section><!-- end main-content -->

        </div><!-- end hoeapp-container-->
    </div><!-- end hoeapp-wrapper-->
    
	<?php include('template-endbody.html'); ?>
	 
</body>

</html>